import discord
from discord.ext import commands

import pyrankvote
from pyrankvote import Candidate, Ballot

import sqlite3

import hato_book_club_env

conn = sqlite3.connect('hato_book_club.db', isolation_level=None)

c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS elections
             (proposal_start TEXT NOT NULL, proposal_end TEXT NOT NULL, vote_start TEXT NOT NULL, vote_end TEXT NOT NULL)''')
c.execute('''CREATE TABLE IF NOT EXISTS books
             (title TEXT NOT NULL, UNIQUE(title))''')
c.execute('''CREATE TABLE IF NOT EXISTS users
             (discord_id TEXT NOT NULL, admin INTEGER NOT NULL DEFAULT 0, UNIQUE(discord_id))''')
c.execute('''CREATE TABLE IF NOT EXISTS election_candidates
             (book_id INTEGER NOT NULL, discord_id TEXT NOT NULL, election_id INTEGER NOT NULL, FOREIGN KEY(book_id) REFERENCES books(rowid), FOREIGN KEY(discord_id) REFERENCES users(discord_id), FOREIGN KEY(election_id) REFERENCES elections(rowid), UNIQUE(book_id, election_id))''')
c.execute('''CREATE TABLE IF NOT EXISTS votes
             (discord_id TEXT NOT NULL, election_id INTEGER NOT NULL, election_candidate_id INTEGER NOT NULL, rank INTEGER NOT NULL, FOREIGN KEY(discord_id) REFERENCES users(discord_id), FOREIGN KEY(election_id) REFERENCES elections(rowid), FOREIGN KEY(election_candidate_id) REFERENCES election_candidates(rowid), UNIQUE(discord_id, election_candidate_id) UNIQUE(discord_id, election_id, rank))''')

description = "A bot that lets you propose and vote on #book-club related things"

intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(command_prefix='?', description=description, intents=intents)

def is_admin(ctx):
  c.execute("SELECT admin FROM users WHERE discord_id=?", (ctx.message.author.id, ))
  is_admin = c.fetchone()[0]
  if is_admin == 1:
    return True
  else:
    return False

def is_member(ctx):
  c.execute("SELECT discord_id FROM users WHERE discord_id=?", (ctx.message.author.id, ))
  is_member = c.fetchone()[0]
  if is_member != '':
    return True
  else:
    return False

@bot.event
async def on_ready():
  print('Logged in as')
  print(bot.user.name)
  print(bot.user.id)
  print('------')

@bot.command()
async def join(ctx):
  """Join the book club to get relevant information and submit requests/vote"""
  c.execute("SELECT discord_id FROM users WHERE discord_id=?", (ctx.message.author.id, ))
  discord_id = c.fetchone()
  if discord_id == None:
    c.execute("INSERT INTO users VALUES (?, ?)", (ctx.message.author.id, 0))
    await ctx.send("Successfully joined book club.")
  else:
    await ctx.send("You are already in the book club!")

@bot.command()
async def start_election(ctx, *args: str):
  """(admin) Start a new election now for the book club with 2 arguments: proposal end AND vote end IN DAYS"""
  if is_admin(ctx):
    c.execute("INSERT INTO elections VALUES (DATETIME('now'), DATETIME('now', '+{} day'), DATETIME('now', '+{} day'), DATETIME('now', '+{} day'))".format(args[0], args[0], args[1]))
    c.execute("SELECT * FROM elections ORDER BY rowid DESC LIMIT 1")
    election = c.fetchone()
    await ctx.send("```Proposal start: {}\nProposal end: {}\nVote start: {}\nVote end: {}```".format(election[0], election[1], election[2], election[3]))
  else:
    await ctx.send("You must be admin to start a new election")

@bot.command()
async def active_elections(ctx):
  """List active elections"""
  c.execute("SELECT DATETIME('now')")
  time_now = c.fetchone()[0]
  c.execute("SELECT rowid, proposal_start, proposal_end, vote_start, vote_end FROM elections WHERE vote_end > ?", (time_now, ))
  elections = c.fetchall()
  for election in elections:
    await ctx.send("```ID: {}\nProposal start: {}\nProposal end: {}\nVote start: {}\nVote end: {}```".format(election[0], election[1], election[2], election[3], election[4]))

@bot.command()
async def add_book(ctx, *term: str):
  """(member) Adds a book that you can propose for an election"""
  if is_member(ctx):
    c.execute("INSERT INTO books VALUES (?)", (' '.join(term), ))
    c.execute("SELECT rowid FROM books ORDER BY rowid DESC LIMIT 1")
    book_id = c.fetchone()[0]
    await ctx.send("Successfully added book to list of books with ID: `{}`".format(book_id))
  else:
    await ctx.send("You must be in the book club to add a book. Join with ?join")

@bot.command()
async def get_book(ctx, *term: str):
  """Get book ID from part or all of its name"""
  c.execute("SELECT rowid, title FROM books WHERE UPPER(title) like UPPER(?)", ['%'+' '.join(term)+'%'])
  book = c.fetchone()
  await ctx.send("```ID: {}\nTitle: {}```".format(book[0], book[1]))

@bot.command()
async def propose(ctx, *id: str):
  """(member) Propose a book by ID to all active elections"""
  if is_member(ctx):
    c.execute("SELECT DATETIME('now')")
    time_now = c.fetchone()[0]
    c.execute("SELECT rowid FROM elections WHERE proposal_end > ?", (time_now, ))
    elections = c.fetchall()
    for election in elections:
      c.execute("INSERT INTO election_candidates VALUES(?, ?, ?)", (int(id[0]), ctx.message.author.id, election[0]))
      c.execute("SELECT rowid FROM election_candidates ORDER BY rowid DESC LIMIT 1")
      election_candidate_id = c.fetchone()[0]
      await ctx.send("Successfully added proposal to election ID: `{}` with election candidate ID: `{}`".format(election[0], election_candidate_id))
  else:
    await ctx.send("You must be in the book club to propose a book. Join with ?join")

@bot.command()
async def proposals(ctx, *id: str):
  """List all proposals for the given election ID"""
  c.execute("SELECT election_candidates.rowid, books.title FROM books INNER JOIN election_candidates ON books.rowid=election_candidates.book_id WHERE election_candidates.election_id = ?", (int(id[0]), ))
  books = c.fetchall()
  send_str = "```"
  for book in books:
    send_str += "ID: " + str(book[0]) + " Title: " + book[1] + "\n"
  send_str += "```"
  await ctx.send(send_str)

@bot.command()
async def vote(ctx, *id: str):
  """Vote for a book in an elections by the given election candidate ID follow by vote rank (example: ?vote 1 3 would vote for candidate 1 with a rank of 3)"""
  c.execute("SELECT election_id FROM election_candidates WHERE rowid = ?", [int(id[0])])
  election_id = c.fetchone()[0]
  c.execute("INSERT INTO votes VALUES(?, ?, ?, ?)", [ctx.message.author.id, election_id, int(id[0]), int(id[1])])
  await ctx.send("Successfully voted for book in election at rank {}".format(id[1]))

@bot.command()
async def my_votes(ctx, *id: str):
  """List your ranked votes in a given election"""
  c.execute("SELECT votes.rank, books.title FROM votes INNER JOIN election_candidates ON votes.election_candidate_id=election_candidates.rowid INNER JOIN books ON election_candidates.book_id=books.rowid WHERE votes.discord_id = ? ORDER BY votes.rank ASC", [ctx.message.author.id])
  votes = c.fetchall()
  send_str = "```VOTE_RANK | BOOK_TITLE\n"
  for vote in votes:
    send_str += str(vote[0]) + " | " + vote[1] + "\n"
  send_str += "```"
  await ctx.send(send_str)

@bot.command()
async def votes(ctx, *id: str):
  """Show votes for books by election ID"""
  c.execute("SELECT election_candidates.rowid, books.title, COUNT(votes.discord_id) FROM election_candidates INNER JOIN books ON election_candidates.book_id=books.rowid INNER JOIN votes ON election_candidates.rowid=votes.election_candidate_id WHERE election_candidates.election_id = ? GROUP BY election_candidates.rowid, books.title", [id[0]])
  results = c.fetchall()
  send_str = "```ELECTION_CANDIDATE_ID | BOOK_TITLE | VOTES\n"
  for line in results:
    send_str += str(line[0]) + " | " + line[1] + " | " + str(line[2]) + "\n"
  send_str += "```"
  await ctx.send(send_str)

@bot.command()
async def vote_results(ctx, *id: str):
  """Show ranked voting results by election ID"""
  candidates = []
  ballots = []
  user_votes = {}
  c.execute("SELECT votes.discord_id, books.title, votes.rank FROM election_candidates INNER JOIN books ON election_candidates.book_id=books.rowid INNER JOIN votes ON election_candidates.rowid=votes.election_candidate_id WHERE election_candidates.election_id = ?", [int(id[0])])
  all_votes = c.fetchall()
  for vote in all_votes:
    if Candidate(vote[1]) not in candidates:
      candidates.append(Candidate(vote[1]))
    if vote[0] in user_votes.keys():
      user_votes[vote[0]][vote[1]] = vote[2]
    else:
      user_votes[vote[0]] = {}
      user_votes[vote[0]][vote[1]] = vote[2]
  for user in user_votes:
    sorted_votes = dict(sorted(user_votes[user].items(), key=lambda item: item[1]))
    my_candidates = []
    for vote in sorted_votes:
      my_candidates.append(Candidate(vote))
    ballots.append(Ballot(ranked_candidates=my_candidates))
  election_result = pyrankvote.instant_runoff_voting(candidates, ballots)
  await ctx.send(election_result)

bot.run(hato_book_club_env.bot_key)